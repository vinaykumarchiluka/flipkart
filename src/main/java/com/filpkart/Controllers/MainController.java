package com.filpkart.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.filpkart.Model.UserRegistration;
import com.filpkart.Service.ServiceImp;

@Controller
public class MainController {

	@Autowired
	private ServiceImp serviceImp;
	
	@RequestMapping(value="/", method= RequestMethod.GET)
	public ModelAndView homePage(ModelAndView mv) {
		
		mv.setViewName("homePage");
		System.out.println("test");
		return mv;
	}
	@RequestMapping(value="/registerpage", method= RequestMethod.GET)
	public ModelAndView registerPage(ModelAndView mv) {
		
		mv.setViewName("RegisterPage");
		System.out.println("register test");
		return mv;
	}
	@RequestMapping(value="/saveRegistration", method= RequestMethod.GET)
	public ModelAndView saveRegistration(ModelAndView mv) {
		
		UserRegistration usr = new UserRegistration();
		usr.setUserName("dummy2");
		serviceImp.saveUserRegistration(usr);
		mv.setViewName("RegisterPage");
		System.out.println("register data");
		return mv;
	}
	
	
}

package com.filpkart.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filpkart.Dao.DaoImpl;
import com.filpkart.Model.UserRegistration;

@Service
public class ServiceImp {

	@Autowired
	private DaoImpl daoImpl;
	
	public void saveUserRegistration(UserRegistration usr) {
		
		daoImpl.saveUserRegistration(usr);
		
	}

}

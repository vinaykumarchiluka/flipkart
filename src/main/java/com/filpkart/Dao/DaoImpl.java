package com.filpkart.Dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.filpkart.Model.UserRegistration;

@Repository
public class DaoImpl {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void saveUserRegistration(UserRegistration usr) {
		System.out.println("dao............"+usr.getUserName());
		Session session = sessionFactory.openSession();
		//session.beginTransaction();
		
		session.save(usr);
		UserRegistration emp = (UserRegistration) session.get(UserRegistration.class, 2);
		 System.out.println(emp.getUserName());
		//session.getTransaction().commit();
		session.flush();
	}

	
}

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>RASSCI</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600'
	rel='stylesheet' type='text/css'>
<link
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700"
	rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<%
	String candidateName = "hello";
	String email = "";//(String) request.getSession().getAttribute("autoLoginName");
	String jrfId = "";//(String) request.getSession().getAttribute("jrfId");
	String iassessmentUrl = "";// TPProperties.newInstance().getProperty(TPProperties.IASSESSMENT_URL);
%>
<script type="text/javascript">
var iassessmentUrl ="<%=iassessmentUrl%>";
	var candidateName =
<%=candidateName%>
	;
	var email =
<%=email%>
	;
	var jrfId =
<%=jrfId%>
	;
</script>
<style>
html, body {
	min-height: 100%;
}

body, div, form, input, p {
	padding: 0;
	margin: 0;
	outline: none;
	font-family: Roboto, Arial, sans-serif;
	font-size: 14px;
	color: #666;
	line-height: 22px;
}

h1 {
	font-weight: 400;
}

.testbox {
	display: flex;
	justify-content: center;
	align-items: center;
	height: inherit;
	padding: 3px;
}

form {
	width: 100%;
	padding: 20px;
	background: #fff;
	box-shadow: 0 2px 5px #ccc;
}

input {
	width: calc(100% - 10px);
	padding: 5px;
	border: 1px solid #ccc;
	border-radius: 3px;
	vertical-align: middle;
}

input:hover, textarea:hover {
	outline: none;
	border: 1px solid #095484;
}

th, td {
	width: 28%;
	padding: 15px 0;
	border-bottom: 1px solid #ccc;
	text-align: center;
	vertical-align: unset;
	line-height: 18px;
	font-weight: 400;
	word-break: break-all;
}

.first-col {
	width: 16%;
	text-align: left;
}

textarea:hover {
	outline: none;
	border: 1px solid #1c87c9;
}

table {
	width: 100%;
}

textarea {
	width: calc(100% - 6px);
}

.question {
	padding: 15px 0 5px;
	color: #095484;
	font-size: 18px;
}

.question-answer label {
	display: block;
	padding: 0 20px 10px 0;
	font-size: 14px;
}

.question-answer input {
	width: auto;
}

.btn-block {
	margin-top: 20px;
	text-align: center;
}

button {
	width: 150px;
	padding: 10px;
	border: none;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	background-color: #095484;
	font-size: 16px;
	color: #fff;
	cursor: pointer;
}

button:hover {
	background-color: #0666a3;
}

@media ( min-width : 568px) {
	th, td {
		word-break: keep-all;
	}
}

.testbox {
	
	display: flex;
	justify-content: center;
	align-items: center;
	height: inherit;
	padding: 3px;
}

form {
	width: 550px;
	padding: 20px;
	background: #fff;
	box-shadow: 0 2px 5px #403f3f;
}

input, select, textarea {
	margin-bottom: 10px;
	border: 1px solid #ccc;
	border-radius: 3px;
}

input:hover, select:hover, textarea:hover {
	outline: none;
	border: 1px solid #095484;
}

input {
	width: calc(100% - 10px);
	padding: 5px;
}

select {
	width: 100%;
	padding: 7px 0;
	background: transparent;
}

textarea {
	width: calc(100% - 6px);
}

.item {
	position: relative;
	margin: 10px 0;
	font-size: 14px;
}

input[type="date"]::-webkit-inner-spin-button {
	display: none;
}

.item i, input[type="date"]::-webkit-calendar-picker-indicator {
	position: absolute;
	font-size: 20px;
	color: #a9a9a9;
}

.item i {
	right: 2%;
	top: 30px;
	z-index: 1;
}

[type="date"]::-webkit-calendar-picker-indicator {
	right: 1%;
	z-index: 2;
	opacity: 0;
	cursor: pointer;
}

.btn-block {
	margin-top: 20px;
	text-align: center;
}

button {
	width: 150px;
	padding: 10px;
	border: none;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	background-color: #095484;
	font-size: 16px;
	color: #fff;
	cursor: pointer;
}

button:hover {
	background-color: #0666a3;
}

.error {
	color: red !important;
	font-weight: normal;
}
</style>
<script>
	function checking() {
		var candidate = $('input[name=visited]:checked').val();

		if (candidate == 'I am interested') {

			$(".candidateDetails").show();
			$(".candidateWorking").hide();
		} else if (candidate == 'Already working /employed') {
			$(".candidateDetails").hide();
			$(".candidateWorking").show();
		} else {
			$(".candidateDetails").hide();
			$(".candidateWorking").hide();
		}

	}
	
	$(document).ready(function() {
		$(".candidateDetails").hide();
		$(".candidateWorking").hide();

		/* $.ajax({
			url : iassessmentUrl + "getStateList",
			type : "POST",
			success : function(response) {
				
				$("#stateId").append(response);
			},
			error : function(xhr, status, error) {
			}
		}); */

	});

	$(function() {
		// Initialize form validation on the registration form.
		// It has the name attribute "registration"
		$("form[name='candidateSurveyDetailsFormId']").validate({
			// Specify validation rules
			rules : {
				// The key name on the left side is the name attribute
				// of an input field. Validation rules are defined
				// on the right side
				visited : "required",
				candidateName : "required",
				email : {
					required : true,
					email : true,
				},
				phoneNumber : {
					required : true,
					number : true,
					minlength : 10,
					maxlength : 10
				},
				state : {
					required : true,
				},
				errorPlacement: function(error, element) {
					  if (element.attr("name") == "PhoneFirst" || element.attr("name") == "PhoneMiddle" || element.attr("name") == "PhoneLast") {
					     error.insertAfter("#requestorPhoneLast");
					  } else {
					     error.insertAfter(element);
					  }
					},

			},
			// Specify validation error messages
			messages : {

				visited : "Please select an option",
				candidateName : "Please enter your name",

				email : "Please enter a valid email address",
				phoneNumber : "Please enter your contact number",
				state : "Please select an option from the list",
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler : function(form) {
				form.submit();
			}
		});
	});
</script>
</head>
<body style="background-color: aliceblue;">

	<div class="testbox">
		<form name="candidateSurveyDetailsFormId"
			action="SaveCandidateSurveyDetails" method="post">
			<h1>FeedBack Response Form</h1>
			<p class="question">Are you looking for a job opportunity in
				Retail Sector?</p>
			<div class="question-answer">
				<label><input type="radio" value="I am interested"
					name="visited" onclick="checking();" /> I am interested</label> <label><input
					type="radio" value="Not Interested" name="visited"
					onclick="checking();" /> Not Interested</label> <label><input
					type="radio" value="Want to pursue further education"
					name="visited" onclick="checking();" /> Want to pursue further
					education</label> <label><input type="radio"
					value="Want to start my own business" name="visited"
					onclick="checking();" /> Want to start my own business</label> <label><input
					type="radio" value="Already working /employed" name="visited"
					onclick="checking();" /> Already working /employed</label>
			</div>
			<div class="candidateDetails">
				<div class="item">
					<p>Name of Candidate</p>
					<div class="name-item">
						<input type="text" name="candidateName" value="<%=candidateName%>"
							placeholder="Name" />
					</div>
				</div>
				<div class="item">
					<p>Email</p>
					<input type="text" name="email" />
				</div>
				<div class="item">
					<p>Phone</p>
					<input type="text" name="phoneNumber" />
				</div>
				<div class="item">
					<p>State</p>
					<select name="state" id="stateId">
						<option value="">*Please select*</option>
						
					</select>
				</div>
			</div>
			<div class="candidateWorking">
				<div class="item">
					<p>Name of Employer</p>
					<div class="name-item">
						<input type="text" value="<%=candidateName%>" name="name"
							placeholder="Name" />
					</div>
				</div>
				<div class="item">
					<p>Date of Joining</p>
					<input type="date" name="name" /> <i class="fas fa-calendar-alt"></i>
				</div>
				<div class="item">
					<p>Salary per month</p>
					<input type="text" name="name" />
				</div>

			</div>
			<div class="btn-block">
				<button type="submit">Submit</button>
			</div>
		</form>
	</div>
</body>
</html>